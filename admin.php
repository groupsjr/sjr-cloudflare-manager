<?php

namespace sjr\cdn\cloudflare;

/**
*	shows Cloudflare Manager setting in admin menu
*	attached to `admin_menu` action
*/
function admin_menu(){
	add_options_page( 'Cloudflare', 'Cloudflare', 'manage_options', 
					  'cloudflare-manager', __NAMESPACE__.'\admin_page' );
}
add_action( 'admin_menu', __NAMESPACE__.'\admin_menu' );

/**
*	renders settings page at /wp-admin/options-general.php?page=cloudflare-manager
*	callback for `add_options_page`
*/
function admin_page(){
	wp_register_style( 'cloudflare-manager', plugins_url('/public/admin/options-general.css', __FILE__) );
	wp_enqueue_style( 'cloudflare-manager' );
	
	$plugin_data = get_plugin_data( __DIR__.'/_plugin.php' );
	
	$vars = array( 'tkn' => get_option('cloudflare-tkn'), 
				   'email' => get_option('cloudflare-email'),
				   'url' => get_option('cloudflare-url'),
				   'error_settings' => FALSE,
				   'version' => $plugin_data['Version'] );

	$default_settings = default_settings();
	$zone_settings = get_transient( 'cloudflare-zone-settings' );

	if( !$zone_settings ){
		$zone_settings = api( 'zone_settings' );
		
		if( $zone_settings->result == 'success' )
			set_transient( 'cloudflare-zone-settings', $zone_settings, 300 );
	}	
	
	$vars['zone_settings'] = $zone_settings;
	
	if( $zone_settings->result == 'success' ){
		$settings = (array) $zone_settings->response->result->objs[0];
		
		// api inconsistency :(
		$settings['devmode'] = $settings['dev_mode'];
		unset( $settings['dev_mode'] );
		
		// store the user configurable options to compare against when updating a setting
		$options = array_flip( array_keys($default_settings) );
		update_option( 'cloudflare-zone-settings-user', array_intersect_key($settings, $options) );
		
		// display settings nicely in 3 columns
		ksort( $settings );
		$settings = array_chunk( $settings, 9, TRUE );
		$vars['settings'] = $settings;
		
		// options that can be toggled
		$vars['options'] = $default_settings;
	}

	echo render( 'admin/options-general', $vars );
}

/**
*	update settings from form data
*	attached to `load-settings_page_cloudflare-manager` action
*/
function admin_update(){
	if( !isset($_POST['_wpnonce']) )
		return;
		
	// update login info
	if( wp_verify_nonce($_POST['_wpnonce'], 'cloudflare-zone-settings') ){
		update_option( 'cloudflare-tkn', stripslashes($_POST['tkn']) );
		update_option( 'cloudflare-email', stripslashes($_POST['email']) );
		update_option( 'cloudflare-url', stripslashes($_POST['url']) );
		
		delete_transient( 'cloudflare-zone-settings' );
	}
	
	// modify zone settings
	if( wp_verify_nonce($_POST['_wpnonce'], 'cloudflare-modify') ){
		$settings = default_settings();
		$response = array();
		
		$user_settings = get_option( 'cloudflare-zone-settings-user' );
		
		foreach( $settings as $a => $options ){
			if( $_POST['modify'][$a] != $user_settings[$a] ){
				$response[$a] = api( $a, array('v' => $_POST['modify'][$a]) );
			}
		}
		
		delete_transient( 'cloudflare-zone-settings' );
		
		add_action( 'cloudflare-modify-response', function() use( $response ){
			echo render( 'admin/options-general-modify-response', array('response' => $response) );
		} );
	}
}
add_action( 'load-settings_page_cloudflare-manager', __NAMESPACE__.'\admin_update' );

/**
*	the configurable settings
*	@return array
*/
function default_settings(){
	return array(
		'async' => array( '0' => 'Off', 'a' => 'Automatic', 'm' => 'Manual' ),
		'cache_lvl' => array( 'agg' => 'Aggressive', 'basic' => 'Basic' ),
		'devmode' => array( 0 => 'Off', 1 => 'On' ),
		'ipv46' => array( 0 => 'Disabled', 1 => 'Enabled' ),
		'minify' => array( '0' => 'Off', '1' => 'JavaScript only', 
						   '2' => 'CSS only ', '3' => 'JavaScript and CSS', 
						   '4' => 'HTML only', '5' => 'JavaScript and HTML', 
						   '6' => 'CSS and HTML', '7' => 'CSS, JavaScript, and HTML' ),
		'sec_lvl' => array( 'help' => 'I\'m under attack! ', 'high' => 'High', 
							'med' => 'Medium', 'low' => 'Low', 'eoff' => 'Essentially Off' )
	);
}

/**
*	add `Settings` link to plugin table
*	@param array
*	@return array
*/
function plugin_action_links( $links ){
	array_unshift( $links, '<a href="'.admin_url('options-general.php?page=cloudflare-manager').'">Settings</a>' );
	return $links;
}
add_filter( 'plugin_action_links_sjr-wp-cloudflare-manager/_plugin.php', __NAMESPACE__.'\plugin_action_links' );

/**
*	simple templating engine
*	@param string template file name in /views/, no traiing php
*	@param array varaibles available to template
*	@return string html
*/
function render( $template, $vars = array() ){
	extract( (array) $vars );
	
	ob_start();
	require __DIR__.'/views/'.$template.'.php';
	$html = ob_get_contents();
	ob_end_clean();
	
	return $html;
}