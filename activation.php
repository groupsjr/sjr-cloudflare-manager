<?php

namespace sjr\cdn\cloudflare;

/**
*	creates cloudflare_purge table on plugin activation
*	callback for `register_activation_hook`
*	`url` column is the page that is viewed on the front end
*	`post_id` is any posts linked to from that page
*/
function activation(){
	require_once ABSPATH.'wp-admin/includes/upgrade.php';
	require_once __DIR__.'/lib/admin-bar.php';
	
	$cloudflare_table = get_table();
	
	$schema = "CREATE TABLE `$cloudflare_table` (
				  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `url` varchar(512) DEFAULT NULL,
				  `post_id` int(11) DEFAULT NULL,
				  PRIMARY KEY (`id`)
			   ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
	 
	dbDelta( $schema );
}

/**
*	callback for `register_uninstall_hook`
*	deletes cloudflare_purge table
*/
function cloudflare_purge_uninstall(){
	$cloudflare_table = get_table();
	
	$schema = "DROP TABLE `$cloudflare_table`";	
}
register_uninstall_hook( __FILE__, 'cloudflare_purge_uninstall' );