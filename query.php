<?php

namespace sjr\cdn\cloudflare;

/**
*	sets admin bar menu item to page title
*	@param array
*	@param WP_Query
*	@return array
*/
function posts_results( $posts, \WP_Query $wp_query ){
	$Admin_Bar = $wp_query->get( 'cloudflare_admin_bar' );
		
	if( $Admin_Bar ){
		if( ($wp_query->is_single() || $wp_query->is_page()) && count($posts) ){
			// set admin bar title to single post or page
			$Admin_Bar->set_title( get_the_title($posts[0]->ID) );
		} else {
			// search pages, tax pages, author.. etc
			$Admin_Bar->set_title( wp_title(' ', FALSE, 'right') );
		}
	}
	
	return $posts;
}
add_filter( 'posts_results', __NAMESPACE__.'\posts_results', 10, 2 );

/**
*	stores current url and posts shown in db, to track where on the site content is showing
*	attached to `posts_results` filter
*	@param array
*	@param WP_Query
*	@return array
*/
function posts_results_track( $posts, \WP_Query $wp_query ){
	if( is_admin() )
		return $posts;
		
	global $wp, $wpdb;
	
	$cloudflare_table = get_table();
	$post_ids = array_map( '\sjr\IDs', $posts );
	
	$current_url = untrailingslashit( site_url($wp->request) );
	if( function_exists('aitch') )
		$current_url = aitch( $current_url, FALSE );
		
	$sql_post_ids = count( $post_ids ) ? implode( ', ', $post_ids ) : "''";
	$sql = $wpdb->prepare( "SELECT post_id 
							FROM $cloudflare_table
							WHERE url = %s
							AND post_id IN( $sql_post_ids )", $current_url );
					
	$res = $wpdb->get_col( $sql );
	$not_set = array_diff( $post_ids, $res );
	
	foreach( $not_set as $post_id ){
		$wpdb->replace( $cloudflare_table, array(
			'post_id' => $post_id,
			'url' => $current_url
		) );
	}

	return $posts;
}
add_filter( 'posts_results', __NAMESPACE__.'\posts_results_track', 10, 2 );

/**
*	sets up the Cloudflare Admin Bar object in the main WP Query
*	@param WP_Query
*	@return WP_Query
*/
function pre_get_posts( \WP_Query $wp_query ){
	if( !is_admin() && $wp_query->is_main_query() )
		$wp_query->set( 'cloudflare_admin_bar', new Admin_Bar );
		
	return $wp_query;
}
add_filter( 'pre_get_posts', __NAMESPACE__.'\pre_get_posts', 10, 2 );