<div class="wrap">
	<h2>Cloudflare Manager</h2>
	<p>version <?php echo $version; ?></p>
	
	<form id="cf-zone-settings" method="post">
		<?php wp_nonce_field( 'cloudflare-zone-settings' ); ?> 
		 
		<label>API Key</label>
		<input type="text" name="tkn" value="<?php echo esc_html( $tkn ); ?>"/>
		
		<label>Email</label>
		<input type="text" name="email" value="<?php echo esc_html( $email ); ?>"/>
		
		<label>URL</label>
		<input type="text" name="url" value="<?php echo esc_html( $url ); ?>"/>
		
		<br class="clear"/>
		<button type="submit">Update</button>
	</form>
	
	<h3>Current Settings</h3>
	<p><a href="https://www.cloudflare.com/docs/client-api.html#s3.7">https://www.cloudflare.com/docs/client-api.html#s3.7</a></p>
	
	<form id="cf-modify" method="post">
		<?php wp_nonce_field( 'cloudflare-modify' ); ?>
		
		<?php if( $zone_settings->result != 'success' ): ?>
			<div class="error"><?php echo $zone_settings->msg; ?></div>
			<?php if( isset($zone_settings->body) ): ?>
			<div id="error-body"><?php echo esc_html( $zone_settings->body ); ?></div>
			<iframe style="height: 500px; width:100%" id="error-frame"></iframe>
			<script type="text/javascript">
				var iframe = jQuery('#error-frame');
				var body = jQuery('#error-body').text();
				// remove body first becasue of js error caused
				jQuery('#error-body').remove();
				setTimeout( function(){
					iframe.contents().find('html').html( body );
				}, 100 );
			</script>
			<?php endif; ?>
		<?php else: ?>
			<?php foreach( $settings as $chunk ): ?>
			<ul class="cf-settings">
				<?php foreach( $chunk as $k=>$v ): ?>
				<li>
					<label><?php echo $k; ?></label>
					
					<?php if( array_key_exists($k, $options) ): ?>
					<select name="modify[<?php echo $k; ?>]">
						<?php foreach( $options[$k] as $val => $option ): ?>
						<option value="<?php echo $val; ?>" <?php if( $val == $v ) echo 'selected="selected"'; ?>><?php echo $option." ($val)"; ?></option>
						<?php endforeach; ?>
					</select>
					<?php else: ?>
						<?php echo $v; ?>
					<?php endif; ?>
				</li>
				<?php endforeach; ?>
			</ul>
			<?php endforeach; ?>
			
			<?php do_action( 'cloudflare-modify-response' ); ?>
			
			<br class="clear"/>
			<button type="submit">Update</button>
		<?php endif; ?>
	</form>
</div>