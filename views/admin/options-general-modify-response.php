<h3>The API returned the following:</h3>
<ul>
	<?php foreach( $response as $action => $return ): ?>
	<li>
		<?php echo $action; ?> : 
		
		<?php if( $return->result == 'success' ): ?>
			Success
		<? else: ?>
			<?php echo $return->msg; ?>
		<?php endif; ?>
	</li>
	<?php endforeach; ?>
</ul>