<?php

namespace sjr\cdn\cloudflare;

/**
*	wrapper to post to https://www.cloudflare.com/api_json.html
*	performs action `sjr\cdn\cloudflare\api`
*	@see https://www.cloudflare.com/docs/client-api.html#s3.7
*	@param string
*	@param array
*	@return object
*/
function api( $action, $values = array() ){
	$args = array( 'tkn' => get_option('cloudflare-tkn'), 
				   'email' => get_option('cloudflare-email'), 
				   'a' => $action );
				   
	switch( $action ){
		case 'fpurge_ts':
			// purge_site
			$params = params( array('z') );
			break;
			
		case 'zone_file_purge':
			// purge_url
			$params = params( array('z', 'url') );
			break;
		
		case 'zone_settings':
			$params = params( array('z') );
			break;
		
		// @TODO use keys from default_settings()?
		case 'async':
		case 'cache_lvl':
		case 'devmode':
		case 'ipv46':
		case 'minify':
		case 'sec_lvl':
			$params = params( array('z') );
			break;
			
		default:
			return;
			break;
	}
	
	$args = array_merge( $args, $params, $values );
	
	$response = wp_remote_post( 'https://www.cloudflare.com/api_json.html', array(
		'body' => $args,
		'user-agent' => 'SJR WP CF Manager 0.4.1'
	) );
	
	// use a format that is standardized 
	if( $response instanceof \WP_Error ){
		$return = (object) array( 'err_code' => $response->get_error_code(),
								  'result' => 'error',
								  'msg' => $response->get_error_message() );
			
	} elseif( $response['response']['code'] == 403 ){
		$return = (object) array( 'body' => $response['body'],
								  'err_code' => 403,
								  'result' => 'error',
								  'msg' => 'Forbidden' );
	} else {
		$return = json_decode( $response['body'] );
	}
	
	if( isset($args['url']) )
		$return->url = $args['url'];
	
	do_action( 'sjr\cdn\cloudflare\api', $args, $return );
	
	return $return;
}

/**
*	default paramaters for api call
*	@param array
*	@return array
*/
function params( $p ){
	$return = array();
	foreach( $p as $param ){
		switch( $param ){
			case 'z':
				$return[$param] = get_option('cloudflare-url');
				break;
			
			case 'url':
				$return[$param] = get_home_url();
				break;
		}
	}
	
	return $return;
}