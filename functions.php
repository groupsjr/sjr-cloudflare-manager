<?php

namespace sjr\cdn\cloudflare;

/**
*
*/
function clear_w3(){
	if( function_exists('w3tc_flush_all') ){
		$ok = w3tc_flush_all();
	}
}

/**
*	removes url / post association from the cloudflare cache map table
*	@param string
*	@return
*/
function delete_url( $url ){
	global $wpdb;
	
	$cloudflare_table = get_table();
	
	if( function_exists('aitch') ){
		$url = aitch( $url, FALSE );
	}
	
	$wpdb->delete( $cloudflare_table, array('url' => $url) );
}

/**
*	gets db table storing which posts are linked from which urls
*	@return string
*/
function get_table(){
	static $table_name = NULL;
	
	if( !$table_name ){
		global $table_prefix;
		$table_name = $table_prefix.'cloudflare_purge';
	}
	
	return $table_name;
}
	
/**
*	gets the associated urls to a post id - pages that link to the single page
*	@param int
*	@return array
*/
function get_urls_for_post( $post_id ){
	global $wpdb;
	
	$cloudflare_table = get_table();
	
	// commented line is cache buster, just in case
	$sql = $wpdb->prepare( "SELECT url 
							FROM $cloudflare_table
							WHERE post_id = %d
							/* %s */", $post_id, md5(time()) );
	$res = $wpdb->get_col( $sql );
	
	array_push( $res, get_permalink($post_id) );
	
	return $res;
}

/**
*	purge entire site
*	@param string
*	@return array
*/
function purge_site(){
	$cloudflare_responses = array();
	$cloudflare_responses[] = api( 'fpurge_ts', array('v' => 1) );
	
	add_filter( 'admin_bar_menu', function(\WP_Admin_Bar $admin_bar) use( $cloudflare_responses ){
		set_messages( $admin_bar, $cloudflare_responses );
	}, 1001 );
	
	return $cloudflare_responses;
}

/**
*	purge single url
*	@param string
*	@return object
*/
function purge_url( $url = '' ){
	if( function_exists('aitch') ){
		$base = get_option( 'cloudflare-url' );
		
		$url = aitch( $url, FALSE );
		$url = 'http://'.$base.$url;
	}
	
	$url = untrailingslashit( $url );
	$return = api( 'zone_file_purge', array('url' => $url) );
	
	if( $return->result == 'success' )
		delete_url( $url );
	
	return $return;
}

/**
*	sets messages for api calls as admin bar items
*	@param WP_Admin_Bar
*	@param array
*/
function set_messages( \WP_Admin_Bar $admin_bar, array $cloudflare_responses ){
	foreach( $cloudflare_responses as $k=>$response ){
		if( $response->result == 'success' ){
			$title = $response->url.' : Purged'; //.print_r( $response, TRUE );
		} else {
			$title = $response->url.' : '.$response->msg;
		}
		
		$admin_bar->add_menu( array(
			'parent' => 'cloudflare-manager',
			'id'   => 'cloudflare-manager-'.$k,
			'meta' => array(),
			'title' => $title ) 
		);
	}
}