# Cloudflare Manager #

This plugin simply purges Cloudflare caches. In particular, this is used when full-page caching is enabled on Cloudflare.