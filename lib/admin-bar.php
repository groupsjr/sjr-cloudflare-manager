<?php

namespace sjr\cdn\cloudflare;

class Admin_Bar{
	private $title = 'this page';			// the title to display in the admin bar menu
	
	/*
	*	adds actions / filters to
	*		- track urls on which a post shows
	*/
	public function __construct(){
		add_action( 'admin_bar_menu', array($this, 'admin_bar_menu'), 1000 );
	}
	
	// filters
	
	/*
	*	sets up the admin bar menu and drop down
	*	@param WP_Admin_Bar
	*	@return WP_Admin_Bar
	*/
	public function admin_bar_menu( \WP_Admin_Bar $admin_bar ){	
		$title = $this->get_title();
				
		$admin_bar->add_menu( array(
			'id'   => 'cloudflare-manager',
			'meta' => array(),
			'title' => 'Cloudflare Purge'
		) );
		
		$purge_url = add_query_arg( array('cloudflare-action' => 'purge-single'), $_SERVER['REQUEST_URI'] );
		$admin_bar->add_menu( array(
			'parent' => 'cloudflare-manager',
			'id' => 'cloudflare-manager-single',
			'title' => 'Purge '.$title,
			'href' => wp_nonce_url( $purge_url, 'purge-single', 'cloudflare-key' ),
			'meta' => array() )
		);
		
		$purge_url = add_query_arg( array('cloudflare-action' => 'purge-site'), $_SERVER['REQUEST_URI'] );
		$admin_bar->add_menu( array(
			'parent' => 'cloudflare-manager',
			'id' => 'cloudflare-manager-site',
			'title' => 'Purge Site',
			'href' => wp_nonce_url( $purge_url, 'purge-site', 'cloudflare-key' ),
			'meta' => array() )
		);
		
		$admin_bar->add_menu( array(
			'parent' => 'cloudflare-manager',
			'id' => 'cloudflare-manager-settings',
			'title' => 'Settings',
			'href' => admin_url( 'options-general.php?page=cloudflare-manager' ),
			'meta' => array() )
		);
	}

	// getters & setters
	
	/*
	*	getter for $title, display on admin bar dropdown
	*	@return string
	*/
	public function get_title(){
		return esc_html( $this->title );
	}
	
	/*
	*	setter for $title, display on admin bar dropdown
	*	@param string
	*/
	public function set_title( $title ){
		$this->title = $title;
	}
}