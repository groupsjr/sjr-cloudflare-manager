<?php
/*
Plugin Name: 	SJR Cloudflare Manager
Plugin URI: 	
Description: 
Author: 		Group SJR | Eric Eaglstun
Text Domain: 
Domain Path:	/lang
Version:		0.7.8
Author URI:		
*/

/**
*	check for php > 5.3 on activation
*/
register_activation_hook( __FILE__, create_function('', '$ver = "5.3"; if( version_compare(phpversion(), $ver, "<") ) die( "This plugin requires PHP version $ver or greater be installed." ); require_once __DIR__."/activation.php"; sjr\cdn\cloudflare\activation();') );

require __DIR__.'/index.php';