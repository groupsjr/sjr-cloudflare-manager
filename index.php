<?php

namespace sjr\cdn\cloudflare;

if( is_admin() )
	require __DIR__.'/admin.php';

require __DIR__.'/api.php';
require __DIR__.'/functions.php';
require __DIR__.'/query.php';

require __DIR__.'/lib/admin-bar.php';

/**
*
*	perform purging 
*	attached to `init` action
*/
function init(){
	if( !isset($_GET['cloudflare-key']) )
		return;
		
	switch( TRUE ){
		// purge single page
		case wp_verify_nonce( $_GET['cloudflare-key'], 'purge-single' ):
			// https://www.cloudflare.com/docs/client-api.html#s4.5
			add_filter( 'posts_results', __NAMESPACE__.'\purge_posts', 10, 2 );
			break;
		
		// purge entire cache
		case wp_verify_nonce( $_GET['cloudflare-key'], 'purge-site' ):
			// https://www.cloudflare.com/docs/client-api.html#s4.4
			purge_site();
			break;
				
		default:
			break;
	}
}
add_action( 'init', __NAMESPACE__.'\init' );

/**
*	triggered from purge single page in front end admin bar
*	attached to `posts_results` filter
*	@param array
*	@param WP_Query
*	@return array
*/
function purge_posts( $posts, \WP_Query $wp_query ){
	$Admin_Bar = $wp_query->get( 'cloudflare_admin_bar' );
	
	if( !$Admin_Bar )
		return $posts;
	
	global $wp;
	
	if( $wp_query->is_single() ){
		$urls = get_urls_for_post( $posts[0]->ID );
	} else {
		$url = site_url( $wp->request );
		$urls = array( $url );
	}
	
	$cloudflare_responses = array();
	
	foreach( $urls as $url )
		$cloudflare_responses[] = purge_url( $url );
	
	add_filter( 'admin_bar_menu', function(\WP_Admin_Bar $admin_bar) use( $cloudflare_responses ){
		set_messages( $admin_bar, $cloudflare_responses );
	}, 1001 );
	
	return $posts;
}

/**
*	purge site on future post is published, new post is published
*	@param int
*/
function post_publish_purge_site( $post_id ){
	clear_w3();
	
	$response = purge_site();
	
	do_action( 'sjr\cdn\cloudflare\post_publish_purge_site', $post_id, $response );
}
add_action( 'auto-draft_to_publish', __NAMESPACE__.'\post_publish_purge_site' );
add_action( 'draft_to_publish', __NAMESPACE__.'\post_publish_purge_site' );
add_action( 'future_to_publish', __NAMESPACE__.'\post_publish_purge_site' );

/**
*	updating post, only clear url and urls that link to it
*	@param int
*	@param WP_Post
*	@param WP_Post
*/
function post_updated_purge_urls( $post_id, $post_after, $post_before ){
	if( in_array($post_after->post_status, array('draft')) )
		return;
	
	if( wp_is_post_revision($post_id) )
		return;
	
	clear_w3();
		
	$responses = array();
	$urls = get_urls_for_post( $post_id );
	
	
	foreach( $urls as $url ){
		$responses[] = array(
			'url' => $url,
			'response' => purge_url( $url )
		);
	}
		
	do_action( 'sjr\cdn\cloudflare\post_updated_purge_urls', $post_id, $responses );
}
add_action( 'post_updated', __NAMESPACE__.'\post_updated_purge_urls', 10, 3 );